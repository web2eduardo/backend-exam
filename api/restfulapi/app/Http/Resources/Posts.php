<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Posts extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'content' => $this->content,
            'image' => $this->image,
            'slug' => $this->slug,
            'update_at' => $this->updated_at,
            'created_at' => $this->created_at,
            'id' => $this->id,
            'user_id' => $this->user_id,
        ];
    }
}
