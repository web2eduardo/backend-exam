<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comments;
use App\Models\Posts;
use App\Http\Resources\Comments as CommentsResource;
use App\Http\Requests\CommentsRequest;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentsRequest $request,$slug){

        $post = Posts::where('slug', $slug)->first();
        $comment = $request->isMethod('put') ? Comments::findorFail($request->id) : new Comments;

        $comment->body = $request->input('body');      
        $comment->commentable_id = $post->id;      
        $comment->commentable_type = 'App\\Post';      
        $comment->user_id = auth('api')->user()->id;
        if ($comment->save()) {
            return new CommentsResource($comment);
        } else {
            return response()->json(array('status' => 'Something went wrong'), 404);
        }        
    }

    public function show_comment_user_slug($slug)
    {
        $comments = Posts::where('slug',$slug)->first()->comments()->get();

        return new CommentsResource($comments);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug, $id){
        $comment = Comments::findorFail($id);

        $comment->body = $request->input('body');      
        if ($comment->save()) {
            return new CommentsResource($comment);
        } else {
            return response()->json(array('status' => 'Something went wrong'), 404);
        }       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug,$id){
        $comments = Comments::findorFail($id);

        if ($comments->delete()) {
            return response()->json(array('status' => 'record deleted successfully'));
        }
    }
}
