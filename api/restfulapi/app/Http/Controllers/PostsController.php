<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Posts;
use App\Http\Resources\Posts as PostsResource;
use App\Http\Requests\PostsRequest;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $posts = Posts::all();

        return PostsResource::collection($posts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostsRequest $request)
    {

        $post = new Posts;

        $post->title = $request->input('title');
        $post->content = $request->input('content');
        $post->image = $request->input('image');
        $post->slug = $this->uniqueSlug(str_replace(' ', '-', strtolower($request->input('title'))));
        $post->user_id = auth('api')->user()->id;
        if ($post->save()) {
            return new PostsResource($post);
        } else {
            return response()->json(array('status' => 'Something went wrong'), 404);
        }
    }


    //Create unique slug
    private function uniqueSlug($slug)
    {
        $count_slug = Posts::where('slug', 'like', $slug . '%')->count();
        if ($count_slug > 0) {
            return $slug . '-' . ($count_slug + 1);
        }

        return $slug;

        // dd($slug);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {

        $post = Posts::where('slug', $slug)->first();
        if ($post) {
            return new PostsResource($post);
        }

        return response()->json(array('error' => 'no record(s) found'), 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $post = Posts::findorFail($request->id);

        $post->title = $request->input('title');
        $post->content = $request->input('content');
        $post->image = $request->input('image');
        $post->slug = $this->uniqueSlug(str_replace(' ', '-', strtolower($request->input('title'))));
        if ($post->save()) {
            return new PostsResource($post);
        } else {
            return response()->json(array('status' => 'Something went wrong'), 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $posts = Posts::findorFail($id);

        if ($posts->delete()) {
            return response()->json(array('status' => 'record deleted successfully'));
        }
    }
}
