<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistrationRequest;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AuthenticateController extends Controller{

    public function registeruser(RegistrationRequest $request){
        $user = new User;

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        if ($user->save()) {
            return response()->json(
                array('id' => $user->id)
            );
        }
    }

    public function authenticateUser(LoginRequest $request){
       $user = User::where('email', $request->email)->first();
       
        if(empty($user->email)){
            throw ValidationException::withMessages([
                'email' => ['These credentials do not match our records.'],
            ]);    
        }

       if(!Hash::check($request->password, $user->password)){
            throw ValidationException::withMessages([
                'password' => ['These credentials do not match our records.'],
            ]);     
       }
      
        return response()->json(array('expires_at' => date('Y-m-d h:i:s', strtotime(date("Y-m-d h:i:s"). ' + 10 days')),'token' => $user->createToken('authToken')->accessToken,'token_type' => 'Bearer'));        
    }

    public function logoutUser(){
        Auth::user()->tokens->each(function ($token, $key) {
          
        });

    }

}


//eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNTI1NzU2MDUxYzRhMWI5ZGU5YWMwZTcxMTQ4MjEzYzRmOTBmNDAzNTliMTBjYmNjYzFlZjgyNGYwOWFjZDE1ZWY2NjIwNDIyZmFhNDFlZDgiLCJpYXQiOjE2MDU2ODAyODcsIm5iZiI6MTYwNTY4MDI4NywiZXhwIjoxNjIxMzE4Njg2LCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.XnYoZZJ5sN9Cd0yyfSz2gNua13Swz4F5tvkShDKVf-pLuw-zBQz3jIDnWYfsgvcu0XceG9vopsH2Hf6DfiPu0yS4rxYmo4t3acKrnefS30M3C4JEAKfXg3SXd6HQmQtfqzgewtUds0piHzzEyr7CmekDaMKoMn-_Yn1nQyqnq2YQt-lZqbSMTOsgU9nD6Ncq2BcsXokK7mS2zGhQR4w7iPLNezGuGQi9RJU6GBmkxBM-Xz-3pZisD6LyY_qygZ-KXEmR6MXaY0uMD8VXq8TMZCHvQ8D3SpZzCcOfNUE_hiHgbZBdN2uCA5puLgT2X0pTKlpUNOwnpIVQld_z40a_Hiaee89m_uR255kV5mv6o00UKHrkHim_FyEezJHT6devd1tTTdnEC6VfIXKfAXVx0J-J7xt696KTmPrJgcNCYAdHs78-SDoz3bdecwpIGRJwiNhCBqFo6l9AqmPO-i7bLKsuFrJqUAveSpyIEJq332eWY6nUsklQRQkGrou6OJaEyDUTst0cJiVc_al_1ghz45ZL6aOgxwYBCxhbbbEF1SzU0r_rHa63JcXxE8AMfFVXLOrHHADa5LIcAEAdtZ8WyAnUET1ULVqPxGp6Ruw-NltN41E5UV7P6hpjqHIuJfZsce3Mm3MTXtuVb_AQ5jihQaYqE09SX9XUYOYM3U4tl5I