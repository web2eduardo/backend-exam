<?php

use App\Http\Controllers\AuthenticateController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\CommentsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->group(function () {

    Route::post('logout', [AuthenticateController::class, 'logoutUser']);

    //Authenticated Post
    Route::post('/posts', [PostsController::class, 'store']);
    Route::patch('/posts/{id}', [PostsController::class, 'update']);
    Route::delete('/posts/{id}', [PostsController::class, 'destroy']);


    //Authenticated Comments
    Route::post('posts/{slug}/comments', [CommentsController::class, 'store']);
    Route::put('posts/{slug}/comments/{comment}', [CommentsController::class, 'update']);
    Route::delete('posts/{slug}/comments/{comment}', [CommentsController::class, 'destroy'])->middleware('auth:api');
});

Route::middleware('guest:api')->group(function () {

    Route::post('register', [AuthenticateController::class, 'registeruser']);
    Route::post('login', [AuthenticateController::class, 'authenticateUser']);


    //Post
    Route::get('/posts', [PostsController::class, 'index']);
    Route::get('/posts/{id}', [PostsController::class, 'show']);


    //Comments
    Route::get('posts/{slug}/comments',[CommentsController::class, 'show_comment_user_slug']);

});
